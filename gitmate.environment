# GitMate example environment file
# ================================

# NOTE: Please change the variables below accordingly.

# Database configuration
# ----------------------

DB_ENGINE=django.db.backends.postgresql_psycopg2
DB_NAME=YOUR_POSTGRES_DB_NAME
DB_USER=YOUR_POSTGRES_USER
DB_PASSWORD=YOUR_POSTGRES_PASSWORD
DB_ADDRESS=db

# Social Auth configuration
# -------------------------

SOCIAL_AUTH_GITHUB_KEY=YOUR_GITHUB_OAUTH_KEY
SOCIAL_AUTH_GITHUB_SECRET=YOUR_GITHUB_OAUTH_SECRET

SOCIAL_AUTH_GITLAB_KEY=YOUR_GITLAB_OAUTH_KEY
SOCIAL_AUTH_GITLAB_SECRET=YOUR_GITLAB_OAUTH_KEY

SOCIAL_AUTH_REDIRECT=https://your.domain.tld
SOCIAL_AUTH_GITLAB_REDIRECT_URL=https://your.domain.tld

# Bot configuration
# -----------------

HOOK_DOMAIN=your.domain.tld

COAFILE_BOT_GITHUB_TOKEN=ignoreme
GITHUB_BOT_USER=ignoreme
WEBHOOK_SECRET=YOUR_GITHUB_WEBHOOK_SECRET

# Celery configuration
# --------------------

CELERY_BROKER_URL=amqp://YOUR_RABBIT_USER:YOUR_RABBIT_PASSWORD@rabbit/
# If set to True, every tasks will be executed by the celery workers
FORCE_CELERY=True

# Docker image configuration
# --------------------------

# Set the tag to "latest" to use the "master" branch.
# WARNING: Instability and security issues may occur if latest is used.
COALA_RESULTS_IMAGE=registry.gitlab.com/gitmate/open-source/coala-incremental-results:release
RESULTS_BOUNCER_IMAGE=registry.gitlab.com/gitmate/open-source/result-bouncer:release

# Django configuration
# --------------------

# If set to True, the stacktrace will show up instead of a regular 5xx page
# WARNING: Secret keys may appear if you set this to True
DJANGO_DEBUG=False
DJANGO_SECRET_KEY=YOUR_DJANGO_SECRET_KEY

# Gunicorn configuration
# ----------------------

# This is the number of *GUNICORN WORKERS* not *CELERY WORKERS*
# We recommend number of cores multiplied by 2
# ex: - Quad-core CPU -> 4 * 2 = 8
#     - Hexa-core CPU -> 6 * 2 = 12
#     - Octa-core CPU -> 8 * 2 = 16
NUM_WORKERS=8
